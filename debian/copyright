Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pktools
Upstream-Contact: Pieter Kempeneers <kempenep@gmail.com>
Source: https://download.savannah.gnu.org/releases/pktools/
Files-Excluded: build/*
 **/CMakeLists.txt.orig
 **/CMakeLists.txt.rej
 *.patch

Files: *
Copyright: 2008-2017, Pieter Kempeneers <kempenep@gmail.com>
License: GPL-3+

Files: qgis/*
Copyright: 2015, Pieter Kempeneers <kempenep@gmail.com>
License: GPL-2+

Files: src/algorithms/Makefile
 src/algorithms/Makefile.in
 src/apps/Makefile
 src/apps/Makefile.in
 src/base/Makefile
 src/base/Makefile.in
 src/fileclasses/Makefile
 src/fileclasses/Makefile.in
 src/imageclasses/Makefile
 src/imageclasses/Makefile.in
 src/lasclasses/Makefile
 src/lasclasses/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: fsf-unlimited-makefile

Files: src/algorithms/myfann_cpp.h
Copyright: 2004-2006, <freegoldbar@yahoo,com>
License: LGPL-2.1+

Files: src/algorithms/svm.cpp
 src/algorithms/svm.h
Copyright: 2000-2012, Chih-Chung Chang and Chih-Jen Lin
License: BSD-3-Clause

Files: debian/*
Copyright: 2013, Francesco Paolo Lovergine <frankie@debian.org>
License: GPL-3+

License: fsf-unlimited-makefile
 This Makefile.in is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Neither name of copyright holders nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1+
 This wrapper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

