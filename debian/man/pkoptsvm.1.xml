<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pkoptsvm'>

  <refmeta>
    <refentrytitle>pkoptsvm</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pkoptsvm</refname>
    <refpurpose>program to optimize parameters for SVM classification</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pkoptsvm</command>
      <arg choice='plain'><option>-t</option> <replaceable>training</replaceable></arg>
      <arg choice='opt'><replaceable>options</replaceable></arg>
      <arg choice='opt'><replaceable>advanced options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pkoptsvm</command> 
      The support vector machine depends on several parameters.
      Ideally, these parameters should be optimized for each classification
      problem.
      In case of a radial basis kernel function, two important parameters are
      {cost} and {gamma}.
      The utility <command>pkoptsvm</command> can optimize these two parameters,
      based on an accuracy assessment (the Kappa value).
      If an input test set (<option>-i</option>) is provided, it is used for
      the accuracy assessment.
      If not, the accuracy assessment is based on a cross validation
      (<option>-cv</option>) of the training sample.
    </para>
    <para>
      The optimization routine uses a grid search.
      The initial and final values of the parameters can be set with
      <option>-cc</option> startvalue <option>-cc</option> endvalue and
      <option>-g</option> startvalue <option>-g</option> endvalue for cost
      and gamma respectively.
      The search uses a multiplicative step for iterating the parameters
      (set with the options <option>-stepcc</option> and
      <option>-stepg</option>).
      An often used approach is to define a relatively large multiplicative
      step first (e.g 10) to obtain an initial estimate for both parameters.
      The estimate can then be optimized by defining a smaller step (&gt;1)
      with constrained start and end values for the parameters cost and gamma.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-t</option> <replaceable>filename</replaceable></term>
        <term><option>--training</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            training vector file.
            A single vector file contains all training features
            (must be set as: b0, b1, b2,...) for all classes
            (class numbers identified by label option).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            input test vector file
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cc</option> <replaceable>startvalue</replaceable> <option>-cc</option> <replaceable>endvalue</replaceable></term>
        <term><option>--ccost</option> <replaceable>startvalue</replaceable> <option>--ccost</option> <replaceable>endvalue</replaceable></term>
        <listitem>
          <para>
            min and max boundaries the parameter C of C-SVC, epsilon-SVR,
            and nu-SVR (optional: initial value)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-g</option> <replaceable>startvalue</replaceable> <option>-g</option> <replaceable>endvalue</replaceable></term>
        <term><option>--gamma</option> <replaceable>startvalue</replaceable> <option>--gamma</option> <replaceable>endvalue</replaceable></term>
        <listitem>
          <para>
            min max boundaries for gamma in kernel function
            (optional: initial value)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-step</option> <replaceable>stepsize</replaceable></term>
        <term><option>--step</option> <replaceable>stepsize</replaceable></term>
        <listitem>
          <para>
            multiplicative step for ccost and gamma in GRID search
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option> <replaceable>level</replaceable></term>
        <term><option>--verbose</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>
            use 1 to output intermediate results for plotting
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    
    <para>Advanced options</para>
    <variablelist>

      <varlistentry>
        <term><option>-tln</option> <replaceable>layer</replaceable></term>
        <term><option>--tln</option> <replaceable>layer</replaceable></term>
        <listitem>
          <para>
            training layer name(s)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-label</option> <replaceable>attribute</replaceable></term>
        <term><option>--label</option> <replaceable>attribute</replaceable></term>
        <listitem>
          <para>
            identifier for class label in training vector file.
            (default: label)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-bal</option> <replaceable>size</replaceable></term>
        <term><option>--balance</option> <replaceable>size</replaceable></term>
        <listitem>
          <para>
            balance the input data to this number of samples for each class
            (default: 0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-random</option></term>
        <term><option>--random</option></term>
        <listitem>
          <para>
            in case of balance, randomize input data
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-min</option> <replaceable>number</replaceable></term>
        <term><option>--min</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            if number of training pixels is less then min,
            do not take this class into account
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-b</option> <replaceable>band</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            band index (starting from 0, either use band option or use start
            to end)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-sband</option> <replaceable>band</replaceable></term>
        <term><option>--startband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            start band sequence number
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eband</option> <replaceable>band</replaceable></term>
        <term><option>--endband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            end band sequence number
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-offset</option> <replaceable>value</replaceable></term>
        <term><option>--offset</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            offset value for each spectral band input features:
            <literal>refl[band]=(DN[band]-offset[band])/scale[band]</literal>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-scale</option> <replaceable>value</replaceable></term>
        <term><option>--scale</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            scale value for each spectral band input features:
            <literal>refl=(DN[band]-offset[band])/scale[band]</literal>
            (use <literal>0</literal> if scale min and max in each band to
            <literal>-1.0</literal> and <literal>1.0</literal>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-svmt</option> <replaceable>type</replaceable></term>
        <term><option>--svmtype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            type of SVM (C_SVC, nu_SVC,one_class, epsilon_SVR, nu_SVR)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kt</option> <replaceable>type</replaceable></term>
        <term><option>--kerneltype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            type of kernel function (linear,polynomial,radial,sigmoid)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kd</option> <replaceable>value</replaceable></term>
        <term><option>--kd</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            degree in kernel function
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c0</option> <replaceable>value</replaceable></term>
        <term><option>--coef0</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            coef0 in kernel function
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nu</option> <replaceable>value</replaceable></term>
        <term><option>--nu</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            the parameter nu of nu-SVC, one-class SVM, and nu-SVR
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eloss</option> <replaceable>value</replaceable></term>
        <term><option>--eloss</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            the epsilon in loss function of epsilon-SVR
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cache</option> <replaceable>number</replaceable></term>
        <term><option>--cache</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            cache memory size in MB
            (default: 100)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-etol</option> <replaceable>value</replaceable></term>
        <term><option>--etol</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            the tolerance of termination criterion
            (default: 0.001)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-shrink</option></term>
        <term><option>--shrink</option></term>
        <listitem>
          <para>
            whether to use the shrinking heuristics 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cv</option> <replaceable>value</replaceable></term>
        <term><option>--cv</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            n-fold cross validation mode (default: 0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cf</option></term>
        <term><option>--cf</option></term>
        <listitem>
          <para>
            use Overall Accuracy instead of kappa
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-maxit</option> <replaceable>number</replaceable></term>
        <term><option>--maxit</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            maximum number of iterations
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-tol</option> <replaceable>value</replaceable></term>
        <term><option>--tolerance</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            relative tolerance for stopping criterion
            (default: 0.0001)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-a</option> <replaceable>value</replaceable></term>
        <term><option>--algorithm</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            GRID, or any optimization algorithm from
            http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c</option> <replaceable>name</replaceable></term>
        <term><option>--class</option> <replaceable>name</replaceable></term>
        <listitem>
          <para>
            list of class names.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-r</option> <replaceable>value</replaceable></term>
        <term><option>--reclass</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            list of class values (use same order as in
            <option>--class</option> option).
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

</refentry>
