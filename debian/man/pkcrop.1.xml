<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pkcrop'>

  <refmeta>
    <refentrytitle>pkcrop</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pkcrop</refname>
    <refpurpose>perform raster data operations on image such as crop, extract and stack bands</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pkcrop</command>
      <arg choice='plain'><option>-i</option> <replaceable>input</replaceable></arg>
      <arg choice='plain'><option>-o</option> <replaceable>output</replaceable></arg>
      <arg choice='opt'><replaceable>options</replaceable></arg>
      <arg choice='opt'><replaceable>advanced options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pkcrop</command> can subset and stack raster images.
      In the spatial domain it can crop a bounding box from a larger image.
      The output bounding box is selected by setting the new corner coordinates
      using the options <option>-ulx</option> <option>-uly</option>
      <option>-lrx</option> <option>-lry</option>.
      Alternatively you can set the new image center (<option>-x</option>
      <option>-y</option>) and size.
      This can be done either in projected coordinates (using the options
      <option>-nx</option> <option>-ny</option>) or in image coordinates
      (using the options <option>-ns</option> <option>-nl</option>).
      You can also use a vector file to set the new bounding box (option
      <option>-e</option>).
      In the spectral domain, <command>pkcrop</command> allows you to select
      individual bands from one or more input image(s).
      Bands are stored in the same order as provided on the command line,
      using the option <option>-b</option>.
      Band numbers start with index 0 (indicating the first band).
      The default is to select all input bands.
      If more input images are provided, the bands are stacked into a
      multi-band image.
      If the bounding boxes or spatial resolution are not identical for all
      input images, you should explicitly set them via the options.
      The <command>pkcrop</command> utility is not suitable to mosaic or
      composite images.
      Consider the utility
      <citerefentry>
        <refentrytitle>pkcomposite</refentrytitle>
        <manvolnum>1</manvolnum>
      </citerefentry>
      instead.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Input image file(s).
            If input contains multiple images, a multi-band output is created
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-o</option> <replaceable>filename</replaceable></term>
        <term><option>--output</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Output image file
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-of</option> <replaceable>out_format</replaceable></term>
        <term><option>--oformat</option> <replaceable>out_format</replaceable></term>
        <listitem>
          <para>
            Output image format (see also
            <citerefentry>
              <refentrytitle>gdal_translate</refentrytitle>
              <manvolnum>1</manvolnum>
            </citerefentry>).
            Empty string: inherit from input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ot</option> <replaceable>type</replaceable></term>
        <term><option>--otype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            Data type for output image
            ({Byte / Int16 / UInt16 / UInt32 / Int32 / Float32 / Float64 / CInt16 / CInt32 / CFloat32 / CFloat64}).
            Empty string: inherit type from input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-b</option> <replaceable>band</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            Band index to crop (leave empty to retain all bands)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-sband</option> <replaceable>band</replaceable></term>
        <term><option>--startband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            Start band sequence number
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eband</option> <replaceable>band</replaceable></term>
        <term><option>--endband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            End band sequence number
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ulx</option> <replaceable>ULX</replaceable></term>
        <term><option>--ulx</option> <replaceable>ULX</replaceable></term>
        <listitem>
          <para>
            Upper left x value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-uly</option> <replaceable>ULY</replaceable></term>
        <term><option>--uly</option> <replaceable>ULY</replaceable></term>
        <listitem>
          <para>
            Upper left y value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-lrx</option> <replaceable>LRX</replaceable></term>
        <term><option>--lrx</option> <replaceable>LRX</replaceable></term>
        <listitem>
          <para>
            Lower right x value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-lry</option> <replaceable>LRY</replaceable></term>
        <term><option>--lry</option> <replaceable>LRY</replaceable></term>
        <listitem>
          <para>
            Lower right y value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-dx</option> <replaceable>xres</replaceable></term>
        <term><option>--dx</option> <replaceable>xres</replaceable></term>
        <listitem>
          <para>
            Output resolution in x (in meter)
            (empty: keep original resolution)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-dy</option> <replaceable>yres</replaceable></term>
        <term><option>--dy</option> <replaceable>yres</replaceable></term>
        <listitem>
          <para>
            Output resolution in y (in meter)
            (empty: keep original resolution)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-r</option> <replaceable>resampling_method</replaceable></term>
        <term><option>--resampling-method</option> <replaceable>resampling_method</replaceable></term>
        <listitem>
          <para>
            Resampling method (near: nearest neighbor,
            bilinear: bi-linear interpolation).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-a_srs</option> <replaceable>EPSG:number</replaceable></term>
        <term><option>--a_srs</option> <replaceable>EPSG:number</replaceable></term>
        <listitem>
          <para>
            Override the spatial reference for the output file
            (leave blank to copy from input file, use epsg:3035
            to use European projection and force to European grid)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nodata</option> <replaceable>value</replaceable></term>
        <term><option>--nodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Nodata value to put in image if out of bounds.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    
    <para>Advanced options</para>
    <variablelist>

      <varlistentry>
        <term><option>-e</option> <replaceable>vector</replaceable></term>
        <term><option>--extent</option> <replaceable>vector</replaceable></term>
        <listitem>
          <para>
            get boundary from extent from polygons in vector file
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cut</option></term>
        <term><option>--crop_to_cutline</option></term>
        <listitem>
          <para>
            Crop the extent of the target dataset to the extent of the cutline
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eo</option> <replaceable>options</replaceable></term>
        <term><option>--eo</option> <replaceable>options</replaceable></term>
        <listitem>
          <para>
            Special extent options controlling rasterization:
            <literal>ATTRIBUTE|CHUNKYSIZE|ALL_TOUCHED|BURN_VALUE_FROM|MERGE_ALG</literal>,
            e.g., <option>-eo</option>
            <literal>ATTRIBUTE=</literal><replaceable>fieldname</replaceable>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-m</option> <replaceable>file</replaceable></term>
        <term><option>--mask</option> <replaceable>file</replaceable></term>
        <listitem>
          <para>
            Use the specified file as a validity mask (0 is nodata)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-msknodata</option> <replaceable>value</replaceable></term>
        <term><option>--msknodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Mask value not to consider for crop
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-mskband</option> <replaceable>value</replaceable></term>
        <term><option>--mskband</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Mask band to read (0 indexed).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-co</option> <replaceable>NAME=VALUE</replaceable></term>
        <term><option>--co</option> <replaceable>NAME=VALUE</replaceable></term>
        <listitem>
          <para>
            Creation option for output file.
            Multiple options can be specified.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-x</option> <replaceable>center_x</replaceable></term>
        <term><option>--x</option> <replaceable>center_x</replaceable></term>
        <listitem>
          <para>
            x-coordinate of image center to crop (in meter)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-y</option> <replaceable>center_y</replaceable></term>
        <term><option>--y</option> <replaceable>center_y</replaceable></term>
        <listitem>
          <para>
            y-coordinate of image center to crop (in meter)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nx</option> <replaceable>size_x</replaceable></term>
        <term><option>--nx</option> <replaceable>size_x</replaceable></term>
        <listitem>
          <para>
            image size in x to crop (in meter)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ny</option> <replaceable>size_y</replaceable></term>
        <term><option>--ny</option> <replaceable>size_y</replaceable></term>
        <listitem>
          <para>
            image size in y to crop (in meter)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ns</option> <replaceable>nsample</replaceable></term>
        <term><option>--ns</option> <replaceable>nsample</replaceable></term>
        <listitem>
          <para>
            number of samples to crop (in pixels)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nl</option> <replaceable>nlines</replaceable></term>
        <term><option>--nl</option> <replaceable>nlines</replaceable></term>
        <listitem>
          <para>
            number of lines to crop (in pixels)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-as</option> <replaceable>min</replaceable> <option>-as</option> <replaceable>max</replaceable></term>
        <term><option>--autoscale</option> <replaceable>min</replaceable> <option>--autoscaleautoscale</option> <replaceable>max</replaceable></term>
        <listitem>
          <para>
            scale output to min and max, e.g.,
            <option>--autoscale</option> <replaceable>0</replaceable>
            <option>--autoscale</option> <replaceable>255</replaceable>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-scale</option> <replaceable>scale</replaceable></term>
        <term><option>--scale</option> <replaceable>scale</replaceable></term>
        <listitem>
          <para>
            output=scale*input+offset
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-off</option> <replaceable>offset</replaceable></term>
        <term><option>--offset</option> <replaceable>offset</replaceable></term>
        <listitem>
          <para>
            output=scale*input+offset
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ct</option> <replaceable>filename</replaceable></term>
        <term><option>--ct</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            colour table in ASCII format having 5 columns: id R G B ALFA
            (0: transparent, 255: solid)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-align</option></term>
        <term><option>--align</option></term>
        <listitem>
          <para>
            Align output bounding box to input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-d</option> <replaceable>description</replaceable></term>
        <term><option>--description</option> <replaceable>description</replaceable></term>
        <listitem>
          <para>
            Set image description
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option></term>
        <term><option>--verbose</option></term>
        <listitem>
          <para>
            verbose
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

  <refsect1 id='example'>
    <title>EXAMPLE</title>

    <example>
      <para>
        Crop the input image to the given bounding box
      </para>

      <screen>
<command>pkcrop</command> <option>-i</option> <replaceable>input.tif</replaceable> <option>-ulx</option> <replaceable>100</replaceable> <option>-uly</option> <replaceable>1000</replaceable> <option>-lrx</option> <replaceable>600</replaceable> <option>-lrx</option> <replaceable>100</replaceable> <option>-o</option> <replaceable>output.tif</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Crop the input image to the envelop of the given polygon and mask all
        pixels outside polygon as 0 (using
        <citerefentry>
          <refentrytitle>gdal_rasterize</refentrytitle>
          <manvolnum>1</manvolnum>
        </citerefentry>)
      </para>

      <screen>
<command>pkcrop</command> <option>-i</option> <replaceable>input.tif</replaceable> <option>-e</option> <replaceable>extent.shp</replaceable> <option>-o</option> <replaceable>output.tif</replaceable>
gdal_rasterize -i -burn 0 -l extent extent.shp output.tif
<command>gdal_rasterize</command> <option>-i</option> <option>-burn</option> <replaceable>0</replaceable> <option>-l</option> <replaceable>extent</replaceable> <replaceable>extent.shp</replaceable> <replaceable>output.tif</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Extract bands 3,2,1 (starting from 0) in that order from multi-band
        raster image <filename>input.tif</filename>
      </para>

      <screen>
<command>pkcrop</command> <option>-i</option> <replaceable>input.tif</replaceable> <option>-b</option> <replaceable>3</replaceable> <option>-b</option> <replaceable>2</replaceable> <option>-b</option> <replaceable>1</replaceable> <option>-o</option> <replaceable>output.tif</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Scale raster floating point image <filename>fimage.tif</filename> with
        factor 100 and write as single byte image with the given colourtable
        (for auto scaling, see next example)
      </para>

      <screen>
<command>pkcrop</command> <option>-i</option> <replaceable>fimage.tif</replaceable> <option>-s</option> <replaceable>100</replaceable> <option>-ot</option> <replaceable>Byte</replaceable> <option>-o</option> <replaceable>bimage.tif</replaceable> <option>-ct</option> <replaceable>colortable.txt</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Automatically scale raster floating point image
        <filename>fimage.tif</filename> to [0:100] and write the output as a
        single byte image with the given colourtable
      </para>

      <screen>
<command>pkcrop</command> <option>-i</option> <replaceable>fimage.tif</replaceable> <option>-as</option> <replaceable>0</replaceable> <option>-as</option> <replaceable>100</replaceable> <option>-ot</option> <replaceable>Byte</replaceable> <option>-o</option> <replaceable>bimage.tif</replaceable> <option>-ct</option> <replaceable>colortable.txt</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Crop raster image <filename>large.tif</filename> to the bounding box of
        raster image <filename>small.tif</filename> and use the same pixel size.
      </para>

      <screen>
<command>pkcrop</command> <option>-i</option> <replaceable>large.tif</replaceable> $(<command>pkinfo</command> <option>-i</option> <replaceable>small.tif</replaceable> <option>-bb</option> <option>-dx</option> <option>-dy</option>) <option>-o</option> <replaceable>output.tif</replaceable>
      </screen>
    </example>

  </refsect1>

  <refsect1 id='see-also'>
    <title>SEE ALSO</title>

    <citerefentry>
      <refentrytitle>pkcomposite</refentrytitle>
      <manvolnum>1</manvolnum>
    </citerefentry>

  </refsect1>

</refentry>
