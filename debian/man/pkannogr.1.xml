<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pkannogr'>

  <refmeta>
    <refentrytitle>pkannogr</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pkannogr</refname>
    <refpurpose>classify vector dataset using Artificial Neural Network</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pkannogr</command>
      <arg choice='plain'><option>-t</option> <replaceable>training</replaceable></arg>
      <arg choice='opt'><option>-i</option> <replaceable>input</replaceable></arg>
      <arg choice='opt'><option>-o</option> <replaceable>output</replaceable></arg>
      <arg choice='opt'><option>-cv</option> <replaceable>value</replaceable></arg>
      <arg choice='opt'><replaceable>options</replaceable></arg>
      <arg choice='opt'><replaceable>advanced options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pkannogr</command> implements an artificial neural network (ANN) to
      solve a supervised classification problem.
      The implementation is based on the open source C++ library
      (<ulink url="http://leenissen.dk/fann/wp/">fann</ulink>).
      Both raster and vector files are supported as input.
      The output will contain the classification result, either in raster or
      vector format, corresponding to the format of the input.
      A training sample must be provided as an OGR vector dataset that contains
      the class labels and the features for each training point.
      The point locations are not considered in the training step.
      You can use the same training sample for classifying different images,
      provided the number of bands of the images are identical.
      Use the utility
      <citerefentry>
        <refentrytitle>pkextract</refentrytitle>
        <manvolnum>1</manvolnum>
      </citerefentry>
      to create a suitable training sample, based on a sample of points or
      polygons.
      For raster output maps you can attach a color table using the option
      <option>-ct</option>.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-t</option> <replaceable>filename</replaceable></term>
        <term><option>--training</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            training vector file.
            A single vector file contains all training features (must be set
            as: B0, B1, B2,...) for all classes (class numbers identified by
            label option).
            Use multiple training files for bootstrap aggregation (alternative
            to the <option>--bag</option> and <option>--bsize</option> options,
            where a random subset is taken from a single training file)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-tln</option> <replaceable>layer</replaceable></term>
        <term><option>--tln</option> <replaceable>layer</replaceable></term>
        <listitem>
          <para>
            training layer name(s)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-label</option> <replaceable>attribute</replaceable></term>
        <term><option>--label</option> <replaceable>attribute</replaceable></term>
        <listitem>
          <para>
            identifier for class label in training vector file.
            (default: label)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-prior</option> <replaceable>value</replaceable></term>
        <term><option>--prior</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            prior probabilities for each class (e.g., <option>-prior</option>
            0.3 <option>-prior</option> 0.3 <option>-prior</option> 0.2 )
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cv</option> <replaceable>value</replaceable></term>
        <term><option>--cv</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            n-fold cross validation mode (default: 0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nn</option> <replaceable>number</replaceable></term>
        <term><option>--nneuron</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            number of neurons in hidden layers in neural network (multiple
            hidden layers are set by defining multiple number of neurons:
            <option>-nn</option> 15 <option>-nn</option> 1, default is one
            hidden layer with 5 neurons)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-m</option> <replaceable>filename</replaceable></term>
        <term><option>--mask</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Only classify within specified mask (vector or raster).
            For raster mask, set nodata values with the option
            <option>--msknodata</option>.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-msknodata</option> <replaceable>value</replaceable></term>
        <term><option>--msknodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            mask value(s) not to consider for classification.
            Values will be taken over in classification image.
            Default is 0.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nodata</option> <replaceable>value</replaceable></term>
        <term><option>--nodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            nodata value to put where image is masked as nodata
            (default: 0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-o</option> <replaceable>filename</replaceable></term>
        <term><option>--output</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            output classification image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ot</option> <replaceable>type</replaceable></term>
        <term><option>--otype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            Data type for output image
            ({Byte / Int16 / UInt16 / UInt32 / Int32 / Float32 / Float64 / CInt16 / CInt32 / CFloat32 / CFloat64}).
            Empty string: inherit type from input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-of</option> <replaceable>GDALformat</replaceable></term>
        <term><option>--oformat</option> <replaceable>GDALformat</replaceable></term>
        <listitem>
          <para>
            Output image format (see also
            <citerefentry>
              <refentrytitle>gdal_translate</refentrytitle>
              <manvolnum>1</manvolnum>
            </citerefentry>).
            Empty string: inherit from input image 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-f</option> <replaceable>OGRformat</replaceable></term>
        <term><option>--f</option> <replaceable>OGRformat</replaceable></term>
        <listitem>
          <para>
           Output ogr format for active training sample
           (default: SQLite) 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ct</option> <replaceable>filename</replaceable></term>
        <term><option>--ct</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            colour table in ASCII format having 5 columns: id R G B ALFA
            (0: transparent, 255: solid) 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-co</option> <replaceable>NAME=VALUE</replaceable></term>
        <term><option>--co</option> <replaceable>NAME=VALUE</replaceable></term>
        <listitem>
          <para>
            Creation option for output file.
            Multiple options can be specified.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c</option> <replaceable>name</replaceable></term>
        <term><option>--class</option> <replaceable>name</replaceable></term>
        <listitem>
          <para>
            list of class names.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-r</option> <replaceable>value</replaceable></term>
        <term><option>--reclass</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            list of class values (use same order as in
            <option>--class</option> option).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option> <replaceable>0|1|2</replaceable></term>
        <term><option>--verbose</option> <replaceable>0|1|2</replaceable></term>
        <listitem>
          <para>
            set to: 0 (results only), 1 (confusion matrix), 2 (debug)
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    
    <para>Advanced options</para>
    <variablelist>

      <varlistentry>
        <term><option>-bal</option> <replaceable>size</replaceable></term>
        <term><option>--balance</option> <replaceable>size</replaceable></term>
        <listitem>
          <para>
            balance the input data to this number of samples for each class
            (default: 0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-min</option> <replaceable>number</replaceable></term>
        <term><option>--min</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            if number of training pixels is less then min, do not take this
            class into account (0: consider all classes)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-b</option> <replaceable>band</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            band index (starting from 0, either use <option>--band</option>
            option or use <option>--start</option> to <option>--end</option>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-sband</option> <replaceable>band</replaceable></term>
        <term><option>--startband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            start band sequence number
            (default: 0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eband</option> <replaceable>band</replaceable></term>
        <term><option>--endband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            end band sequence number
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-offset</option> <replaceable>value</replaceable></term>
        <term><option>--offset</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            offset value for each spectral band input features:
            <literal>refl[band]=(DN[band]-offset[band])/scale[band]</literal>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-scale</option> <replaceable>value</replaceable></term>
        <term><option>--scale</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            scale value for each spectral band input features:
            <literal>refl=(DN[band]-offset[band])/scale[band]</literal>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-a</option> <replaceable>1|2</replaceable></term>
        <term><option>--aggreg</option> <replaceable>1|2</replaceable></term>
        <listitem>
          <para>
            how to combine aggregated classifiers, see also
            <option>--rc</option> option (1: sum rule, 2: max rule).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--connection</option> <replaceable>0|1</replaceable></term>
        <listitem>
          <para>
            connection rate (default: 1.0 for a fully connected network)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-w</option> <replaceable>weights</replaceable></term>
        <term><option>--weights</option> <replaceable>weights</replaceable></term>
        <listitem>
          <para>
            weights for neural network.
            Apply to fully connected network only, starting from first input
            neuron to last output neuron, including the bias neurons (last
            neuron in each but last layer)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-l</option> <replaceable>rate</replaceable></term>
        <term><option>--learning</option> <replaceable>rate</replaceable></term>
        <listitem>
          <para>
            learning rate (default: 0.7)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--maxit</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            number of maximum iterations (epoch) (default: 500)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-comb</option> <replaceable>rule</replaceable></term>
        <term><option>--comb</option> <replaceable>rule</replaceable></term>
        <listitem>
          <para>
            how to combine bootstrap aggregation classifiers
            (0: sum rule, 1: product rule, 2: max rule).
            Also used to aggregate classes with <option>--rc</option> option.
            Default is sum rule (0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-bag</option> <replaceable>value</replaceable></term>
        <term><option>--bag</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Number of bootstrap aggregations (default is no bagging: 1)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-bs</option> <replaceable>value</replaceable></term>
        <term><option>--bsize</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Percentage of features used from available training features for
            each bootstrap aggregation (one size for all classes, or a
            different size for each class respectively. default: 100)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cb</option> <replaceable>filename</replaceable></term>
        <term><option>--classbag</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            output for each individual bootstrap aggregation (default is blank)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--prob</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            probability image.
            Default is no probability image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-na</option> <replaceable>number</replaceable></term>
        <term><option>--na</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            number of active training points
            (default: 1)
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

</refentry>
