<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pksvmogr'>

  <refmeta>
    <refentrytitle>pksvmogr</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pksvmogr</refname>
    <refpurpose>classify vector dataset using Support Vector Machine</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pksvmogr</command>
      <arg choice='plain'><option>-t</option> <replaceable>training</replaceable></arg>
      <arg choice='opt'><option>-i</option> <replaceable>input</replaceable></arg>
      <arg choice='opt'><option>-o</option> <replaceable>output</replaceable></arg>
      <arg choice='opt'><option>-cv</option> <replaceable>value</replaceable></arg>
      <arg choice='opt'><replaceable>options</replaceable></arg>
      <arg choice='opt'><replaceable>advanced options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pksvmogr</command> implements a support vector machine (SVM) to
      solve a supervised classification problem.
      The implementation is based on the open source C++ library libSVM
      (http://www.csie.ntu.edu.tw/~cjlin/libsvm).
      Both raster and vector files are supported as input.
      The output will contain the classification result, either in raster or
      vector format, corresponding to the format of the input.
      A training sample must be provided as an OGR vector dataset that
      contains the class labels and the features for each training point.
      The point locations are not considered in the training step.
      You can use the same training sample for classifying different images,
      provided the number of bands of the images are identical.
      Use the utility pkextract to create a suitable training sample, based
      on a sample of points or polygons.
      For raster output maps you can attach a color table using the option
      <option>-ct</option>.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-t</option> <replaceable>filename</replaceable></term>
        <term><option>--training</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Training vector file.
            A single vector file contains all training features
            (must be set as: b0, b1, b2,...) for all classes
            (class numbers identified by label option).
            Use multiple training files for bootstrap aggregation
            (alternative to the <option>--bag</option> and
            <option>--bagsize</option> options, where a random subset
            is taken from a single training file)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-o</option> <replaceable>filename</replaceable></term>
        <term><option>--output</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Output classification image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cv</option> <replaceable>value</replaceable></term>
        <term><option>--cv</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            N-fold cross validation mode (default: 0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-tln</option> <replaceable>layer</replaceable></term>
        <term><option>--tln</option> <replaceable>layer</replaceable></term>
        <listitem>
          <para>
            Training layer name(s)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c</option> <replaceable>name</replaceable></term>
        <term><option>--class</option> <replaceable>name</replaceable></term>
        <listitem>
          <para>
            List of class names.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-r</option> <replaceable>value</replaceable></term>
        <term><option>--reclass</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            List of class values (use same order as in
            <option>--class</option> option).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-of</option> <replaceable>GDALformat</replaceable></term>
        <term><option>--oformat</option> <replaceable>GDALformat</replaceable></term>
        <listitem>
          <para>
            Output image format (see also
            <citerefentry>
              <refentrytitle>gdal_translate</refentrytitle>
              <manvolnum>1</manvolnum>
            </citerefentry>).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-f</option> <replaceable>format</replaceable></term>
        <term><option>--f</option> <replaceable>format</replaceable></term>
        <listitem>
          <para>
            Output ogr format for active training sample
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-co</option> <replaceable>NAME=VALUE</replaceable></term>
        <term><option>--co</option> <replaceable>NAME=VALUE</replaceable></term>
        <listitem>
          <para>
            Creation option for output file.
            Multiple options can be specified.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ct</option> <replaceable>filename</replaceable></term>
        <term><option>--ct</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Color table in ASCII format having 5 columns:
            id R G B ALFA (0: transparent, 255: solid) 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-label</option> <replaceable>attribute</replaceable></term>
        <term><option>--label</option> <replaceable>attribute</replaceable></term>
        <listitem>
          <para>
            Identifier for class label in training vector file.
            (default: label)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-prior</option> <replaceable>value</replaceable></term>
        <term><option>--prior</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Prior probabilities for each class (e.g.,
            <option>-prior</option> 0.3 <option>-prior</option> 0.3
            <option>-prior</option> 0.2)
            Used for input only (ignored for cross validation)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-g</option> <replaceable>gamma</replaceable></term>
        <term><option>--gamma</option> <replaceable>gamma</replaceable></term>
        <listitem>
          <para>
            Gamma in kernel function
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cc</option> <replaceable>cost</replaceable></term>
        <term><option>--ccost</option> <replaceable>cost</replaceable></term>
        <listitem>
          <para>
            The parameter C of C_SVC, epsilon_SVR, and nu_SVR
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-m</option> <replaceable>filename</replaceable></term>
        <term><option>--mask</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Only classify within specified mask (vector or raster).
            For raster mask, set nodata values with the option
            <option>--msknodata</option>.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-msknodata</option> <replaceable>value</replaceable></term>
        <term><option>--msknodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
           Mask value(s) not to consider for classification.
           Values will be taken over in classification image.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nodata</option> <replaceable>value</replaceable></term>
        <term><option>--nodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Nodata value to put where image is masked as nodata
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option> <replaceable>level</replaceable></term>
        <term><option>--verbose</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>
            Verbose level
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    
    <para>Advanced options</para>
    <variablelist>

      <varlistentry>
        <term><option>-b</option> <replaceable>band</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            Band index (starting from 0, either use <option>--band</option>
            option or use <option>--startband</option> to
            <option>--endband</option>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-sband</option> <replaceable>band</replaceable></term>
        <term><option>--startband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            Start band sequence number
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eband</option> <replaceable>band</replaceable></term>
        <term><option>--endband</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            End band sequence number
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-bal</option> <replaceable>size</replaceable></term>
        <term><option>--balance</option> <replaceable>size</replaceable></term>
        <listitem>
          <para>
            Balance the input data to this number of samples for each class
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-min</option> <replaceable>number</replaceable></term>
        <term><option>--min</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            If number of training pixels is less then min, do not take this
            class into account (0: consider all classes)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-bag</option> <replaceable>value</replaceable></term>
        <term><option>--bag</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Number of bootstrap aggregations (default is no bagging: 1)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-bagsize</option> <replaceable>value</replaceable></term>
        <term><option>--bagsize</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Percentage of features used from available training features for
            each bootstrap aggregation (one size for all classes, or a
            different size for each class respectively
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-comb</option> <replaceable>rule</replaceable></term>
        <term><option>--comb</option> <replaceable>rule</replaceable></term>
        <listitem>
          <para>
            How to combine bootstrap aggregation classifiers
            (0: sum rule, 1: product rule, 2: max rule).
            Also used to aggregate classes with rc option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cb</option> <replaceable>filename</replaceable></term>
        <term><option>--classbag</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Output for each individual bootstrap aggregation
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-prob</option> <replaceable>filename</replaceable></term>
        <term><option>--prob</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Probability image.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-offset</option> <replaceable>value</replaceable></term>
        <term><option>--offset</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Offset value for each spectral band input features:
            <literal>refl[band]=(DN[band]-offset[band])/scale[band]</literal>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-scale</option> <replaceable>value</replaceable></term>
        <term><option>--scale</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Scale value for each spectral band input features:
            <literal>refl=(DN[band]-offset[band])/scale[band]</literal>
            (use <literal>0</literal> if scale min and max in each band to
            <literal>-1.0</literal> and <literal>1.0</literal>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-svmt</option> <replaceable>type</replaceable></term>
        <term><option>--svmtype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            Type of SVM (C_SVC, nu_SVC,one_class, epsilon_SVR, nu_SVR)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kt</option> <replaceable>type</replaceable></term>
        <term><option>--kerneltype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            Type of kernel function (linear,polynomial,radial,sigmoid)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kd</option> <replaceable>value</replaceable></term>
        <term><option>--kd</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Degree in kernel function
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c0</option> <replaceable>value</replaceable></term>
        <term><option>--coef0</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Coef0 in kernel function
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nu</option> <replaceable>value</replaceable></term>
        <term><option>--nu</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            The parameter nu of nu-SVC, one-class SVM, and nu-SVR
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eloss</option> <replaceable>value</replaceable></term>
        <term><option>--eloss</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            The epsilon in loss function of epsilon-SVR
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cache</option> <replaceable>number</replaceable></term>
        <term><option>--cache</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            <ulink url="http://pktools.nongnu.org/html/classCache.html">Cache</ulink>
            memory size in MB
            (default: 100)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-etol</option> <replaceable>value</replaceable></term>
        <term><option>--etol</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            the tolerance of termination criterion
            (default: 0.001)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-shrink</option></term>
        <term><option>--shrink</option></term>
        <listitem>
          <para>
            Whether to use the shrinking heuristics 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-na</option> <replaceable>number</replaceable></term>
        <term><option>--nactive</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            Number of active training points
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

</refentry>
