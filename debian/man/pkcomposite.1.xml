<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pkcomposite'>

  <refmeta>
    <refentrytitle>pkcomposite</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pkcomposite</refname>
    <refpurpose>program to mosaic and composite geo-referenced images</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pkcomposite</command>
      <arg choice='plain'><option>-i</option> <replaceable>input</replaceable></arg>
      <arg choice='opt'><option>-i</option> <replaceable>input</replaceable></arg>
      <arg choice='plain'><option>-o</option> <replaceable>output</replaceable></arg>
      <arg choice='opt'><replaceable>options</replaceable></arg>
      <arg choice='opt'><replaceable>advanced options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pkcomposite</command> can be used to {mosaic} and {composite}
      multiple (georeferenced) raster datasets.
      A mosaic can merge images with different geographical extents into a
      single larger image.
      Compositing resolves the overlapping pixels according to some rule
      (e.g, the median of all overlapping pixels).
      This utility is complementary to GDAL, which currently does not support
      a composite step.
      Input datasets can have different bounding boxes and spatial resolutions.
    </para>
    <example>
      <para>
        Example: Calculate the maximum NDVI composite of two multispectral
        input images (e.g., red is band 0 and near infrared is band 1)
      </para>
      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> <option>-cr</option> <replaceable>maxndvi</replaceable> <option>-cb</option> <replaceable>0</replaceable> <option>-cb</option> <replaceable>1</replaceable>
      </screen>
    </example>
    <example>
      <para>
        Example: Calculate the minimum nadir composite of two input images,
        where the forth band (b=3) contains the view zenith angle
      </para>
      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-o</option> <replaceable>minzenith.tif</replaceable> <option>-cr</option> <replaceable>minband</replaceable> <option>-cb</option> <replaceable>3</replaceable>
      </screen>
    </example>
    <example>
      <para>
        Example: Calculate the minimum of two input images in all bands
      </para>
      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-o</option> <replaceable>minimum.tif</replaceable> <option>-cr</option> <replaceable>minallbands</replaceable>
      </screen>
    </example>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Input image file(s).
            If input contains multiple images, a multi-band output is created
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-o</option> <replaceable>filename</replaceable></term>
        <term><option>--output</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Output image file
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-b</option> <replaceable>band</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            band index(es) to crop (leave empty if all bands must be retained)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-dx</option> <replaceable>xres</replaceable></term>
        <term><option>--dx</option> <replaceable>xres</replaceable></term>
        <listitem>
          <para>
            Output resolution in x (in meter)
            (empty: keep original resolution)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-dy</option> <replaceable>yres</replaceable></term>
        <term><option>--dy</option> <replaceable>yres</replaceable></term>
        <listitem>
          <para>
            Output resolution in y (in meter)
            (empty: keep original resolution) 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-e</option> <replaceable>vector</replaceable></term>
        <term><option>--extent</option> <replaceable>vector</replaceable></term>
        <listitem>
          <para>
            get boundary from extent from polygons in vector file
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cut</option></term>
        <term><option>--crop_to_cutline</option></term>
        <listitem>
          <para>
            Crop the extent of the target dataset to the extent of the cutline
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-eo</option> <replaceable>options</replaceable></term>
        <term><option>--eo</option> <replaceable>options</replaceable></term>
        <listitem>
          <para>
            Special extent options controlling rasterization:
            <literal>ATTRIBUTE|CHUNKYSIZE|ALL_TOUCHED|BURN_VALUE_FROM|MERGE_ALG</literal>,
            e.g., <option>-eo</option>
            <literal>ATTRIBUTE=</literal><replaceable>fieldname</replaceable>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-m</option> <replaceable>mask</replaceable></term>
        <term><option>--mask</option> <replaceable>mask</replaceable></term>
        <listitem>
          <para>
            Use the first band of the specified file as a validity mask
            (0 is nodata)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-msknodata</option> <replaceable>value</replaceable></term>
        <term><option>--msknodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Mask value not to consider for composite
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-mskband</option> <replaceable>value</replaceable></term>
        <term><option>--mskband</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Mask band to read
            (0 indexed)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ulx</option> <replaceable>ULX</replaceable></term>
        <term><option>--ulx</option> <replaceable>ULX</replaceable></term>
        <listitem>
          <para>
            Upper left x value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-uly</option> <replaceable>ULY</replaceable></term>
        <term><option>--uly</option> <replaceable>ULY</replaceable></term>
        <listitem>
          <para>
            Upper left y value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-lrx</option> <replaceable>LRX</replaceable></term>
        <term><option>--lrx</option> <replaceable>LRX</replaceable></term>
        <listitem>
          <para>
            Lower right x value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-lry</option> <replaceable>LRY</replaceable></term>
        <term><option>--lry</option> <replaceable>LRY</replaceable></term>
        <listitem>
          <para>
            Lower right y value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cr</option> <replaceable>rule</replaceable></term>
        <term><option>--crule</option> <replaceable>rule</replaceable></term>
        <listitem>
          <para>
            Composite rule (overwrite, maxndvi, maxband, minband, mean, mode
            (only for byte images), median, sum
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cb</option> <replaceable>band</replaceable></term>
        <term><option>--cb</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            band index used for the composite rule (e.g., for ndvi, use
            <option>--cband=0</option> <option>--cband=1</option> with 0 and 1
            indices for red and nir band respectively 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-srcnodata</option> <replaceable>value</replaceable></term>
        <term><option>--srcnodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            invalid value for input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-bndnodata</option> <replaceable>band</replaceable></term>
        <term><option>--bndnodata</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            Bands in input image to check if pixel is valid
            (used for srcnodata, min and max options)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-min</option> <replaceable>value</replaceable></term>
        <term><option>--min</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            flag values smaller or equal to this value as invalid.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-max</option> <replaceable>value</replaceable></term>
        <term><option>--max</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            flag values larger or equal to this value as invalid.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-dstnodata</option> <replaceable>value</replaceable></term>
        <term><option>--dstnodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            nodata value to put in output image if not valid or out of bounds. 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-r</option> <replaceable>resampling_method</replaceable></term>
        <term><option>--resampling-method</option> <replaceable>resampling_method</replaceable></term>
        <listitem>
          <para>
            Resampling method (near: nearest neighbor,
            bilinear: bi-linear interpolation).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ot</option> <replaceable>type</replaceable></term>
        <term><option>--otype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            Data type for output image
            ({Byte / Int16 / UInt16 / UInt32 / Int32 / Float32 / Float64 / CInt16 / CInt32 / CFloat32 / CFloat64}).
            Empty string: inherit type from input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-of</option> <replaceable>GDALformat</replaceable></term>
        <term><option>--oformat</option> <replaceable>GDALformat</replaceable></term>
        <listitem>
          <para>
            Output image format (see also
            <citerefentry>
              <refentrytitle>gdal_translate</refentrytitle>
              <manvolnum>1</manvolnum>
            </citerefentry>).
            Empty string: inherit from input image 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-co</option> <replaceable>NAME=VALUE</replaceable></term>
        <term><option>--co</option> <replaceable>NAME=VALUE</replaceable></term>
        <listitem>
          <para>
            Creation option for output file.
            Multiple options can be specified.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-a_srs</option> <replaceable>EPSG:number</replaceable></term>
        <term><option>--a_srs</option> <replaceable>EPSG:number</replaceable></term>
        <listitem>
          <para>
            Override the spatial reference for the output file
            (leave blank to copy from input file, use epsg:3035
            to use European projection and force to European grid)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option></term>
        <term><option>--verbose</option></term>
        <listitem>
          <para>
            verbose
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    
    <para>Advanced options</para>
    <variablelist>

      <varlistentry>
        <term><option>-file</option></term>
        <term><option>--file</option></term>
        <listitem>
          <para>
            write number of observations (1) or sequence nr of selected file (2)
            for each pixels as additional layer in composite.
            Default: 0
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-w</option> <replaceable>weight</replaceable></term>
        <term><option>--weight</option> <replaceable>weight</replaceable></term>
        <listitem>
          <para>
            Weights (type: short) for the composite, use one weight for each
            input file in same order as input files are provided).
            Use value 1 for equal weights.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c</option> <replaceable>name</replaceable></term>
        <term><option>--class</option> <replaceable>name</replaceable></term>
        <listitem>
          <para>
            classes for multi-band output image: each band represents the
            number of observations for one specific class.
            Use value 0 for no multi-band output image. 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ct</option> <replaceable>filename</replaceable></term>
        <term><option>--ct</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            colour table in ASCII format having 5 columns: id R G B ALFA
            (0: transparent, 255: solid) 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-align</option></term>
        <term><option>--align</option></term>
        <listitem>
          <para>
            Align output bounding box to first input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-scale</option> <replaceable>value</replaceable></term>
        <term><option>--scale</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Scale value
            <literal>output=scale*input+offset</literal>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-off</option> <replaceable>value</replaceable></term>
        <term><option>--offset</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Offset value
            <literal>output=scale*input+offset</literal>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-d</option> <replaceable>description</replaceable></term>
        <term><option>--description</option> <replaceable>description</replaceable></term>
        <listitem>
          <para>
            Set image description
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

  <refsect1 id='example'>
    <title>EXAMPLE</title>

    <example>
      <para>
        Create a composit from two input images. If images overlap, keep only
        last image (default rule)
      </para>

      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Create a composit from two input images.
        Values of 255 in band 1 (starting from 0) are masked as invalid.
        Typically used when second band of input image is a cloud mask
      </para>

      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-srcnodata</option> <replaceable>255</replaceable> <option>-bndnodata</option> <replaceable>1</replaceable> <option>-dstnodata</option> <replaceable>0</replaceable> <option>-o</option> <replaceable>output.tif</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Create a maximum NDVI (normalized difference vegetation index) composit.
        Values of 255 in band 0 are masked as invalid and flagged as 0 if no
        other valid coverage.
        Typically used for (e.g., MODIS) images where red and near infrared
        spectral bands are stored in bands 0 and 1 respectively.
        In this particular case, a value of 255 in the first input band
        indicates a nodata value (e.g., cloud mask is coded within the data
        values).
      </para>

      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-cr</option> <replaceable>maxndvi</replaceable> <option>-rb</option> <replaceable>0</replaceable> <option>-rb</option> <replaceable>1</replaceable> <option>-srcnodata</option> <replaceable>255</replaceable> <option>-bndnodata</option> <replaceable>0</replaceable> <option>-dstnodata</option> <replaceable>0</replaceable> <option>-o</option> <replaceable>output.tif</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Create a composite image using weighted mean:
        output=(3/4*input1+6/4*input2+3/4*input2)/3.0
      </para>

      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-i</option> <replaceable>input3.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> <option>-cr</option> <replaceable>mean</replaceable> <option>-w</option> <replaceable>0.75</replaceable> <option>-w</option> <replaceable>1.5</replaceable> <option>-w</option> <replaceable>0.75</replaceable>
      </screen>
    </example>

    <example>
      <para>
        Create a median composit of all GTiff images found in current directory
        that cover (at least part of) the image
        <filename>coverage.tif</filename>.
        Values smaller or equal to 0 are set as nodata 0 (default value for
        <option>-dstnodata</option>)
      </para>

      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>large.tif</replaceable> $(for IMAGE in *.tif;do <command>pkinfo</command> <option>-i</option> $IMAGE <option>--cover</option> $(<command>pkinfo</command> <option>-i</option> <replaceable>coverage.tif</replaceable> <option>-bb</option>);done) <option>-cr</option> <replaceable>median</replaceable> <option>-min</option> <replaceable>0</replaceable> <option>-o</option> <replaceable>output.tif</replaceable>
      </screen>
    </example>

  </refsect1>

  <refsect1 id='faq'>
    <title>FAQ</title>

    <para>
     Q1. First question
    </para>
    <para>
      A1. For individual invalid value(s) in input image, use
      <option>-srcnodata</option>
    </para>
    <para>
      Usage: use unique value for each invalid bands set in
      <option>--bndnodata</option> or use a single value that
      will be applied to all invalid bands
    </para>
    <para>
      Example:
      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> <option>-srcnodata</option> <replaceable>0</replaceable> <option>-srcnodata</option> <replaceable>255</replaceable> <option>-bndnodata</option> <replaceable>0</replaceable> <option>-bndnodata</option> <replaceable>1</replaceable>
      </screen>
      will consider 0 in band 0 and 255 in band 1 of input images as no value
    </para>
    <para>
      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> <option>-srcnodata</option> <replaceable>0</replaceable> <option>-bndnodata</option> <replaceable>0</replaceable> <option>-bndnodata</option> <replaceable>1</replaceable>
      </screen>
      will consider 0 in both bands 0 and 1 of input images as no value
    </para>
    <para>
      For range(s) of invalid values in input images: use
      <option>-min</option> (<option>--min</option>) and
      <option>-max</option> (<option>--max</option>)
      Usage: use unique range set for each invalid bands set in
      <option>-bndnodata</option>
    </para>
    <para>
      Example:
      <screen>
<command>pkcomposite</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-i</option> <replaceable>input2.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> <option>-min</option> <replaceable>0</replaceable> <option>-max</option> <replaceable>200</replaceable> <option>-min</option> <replaceable>0</replaceable> <option>-max</option> <replaceable>2</replaceable> <option>-bndnodata</option> <replaceable>0</replaceable> <option>-bndnodata</option> <replaceable>1</replaceable>
      </screen>
      will consider all negative values in band 0 and 1 of input images as
      invalid.
      Values larger or equal to 200 in band 0 will be invalid,
      as well as values larger or equal to 2 in band 1
    </para>

    <para>
      Q2. If I take the mean value as composit rule for multi-band input
      images, will the output image contain the mean value of overlapping
      images in each band?
    </para>
    <para>
      A2. Yes
    </para>

  </refsect1>

</refentry>
