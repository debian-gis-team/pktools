\section examples_pkextract Examples of pkextractogr and pkextractimg

\subsection example_pkextract_vector Using vector samples

Extract all points for all layers read in points.sqlite from input.tif. Create a new point vector dataset named extracted.sqlite, where each point will contain an attribute for the individual input bands in input.tif. Notice that the default vector format is Spatialite (.sqlite).

\code
pkextractogr -i input.tif -s points.sqlite -o extracted.sqlite
\endcode

Same example as above, but only extract the points for the layer in points.sqlite named "valid"

\code
pkextractogr -i input.tif -s points.sqlite -ln valid -o extracted.sqlite
\endcode

Extract points and write output in ESRI Shapefile format

\code
pkextractogr -i input.tif -s points.shp -f "ESRI Shapefile" -o extracted.shp
\endcode

Extract the standard deviation for each input band in a 3 by 3 window, centered around the points in the sample vector dataset points.sqlite. The output vector dataset will contain polygon features defined by the buffered points (3x3 window). Use the option -circ to define a circular buffer.

\code
pkextractogr -i input.tif -s points.sqlite -o extracted.sqlite -r stdev -buf 3 -polygon
\endcode

Extract all pixels from input.tif covered by the polygons in locations.sqlite. Each polygon can thus result in multiple point features with attributes for each input band. Write the extracted points to a point vector dataset training.sqlite.

\code
pkextractogr -i input.tif -s polygons.sqlite -o training.sqlite -r point
\endcode

Extract the first band from input.tif at the centroids of the polygons in vector dataset polygons.sqlite. Assign the extracted point value to a new attribute of the polygon and write to the vector dataset extracted.sqlite.

\code
pkextractogr -i input.tif -b 0 -s polygons.sqlite -r centroid -o extracted.sqlite -polygon  
\endcode
Extract the mean values for the second band in input.tif covered by each polygon in polygons.sqlite. The mean values are written to a copy of the polygons in output vector dataset extracted.sqlite

\code
pkextractogr -i input.tif -b 1 -s polygons.sqlite -r mean -o extracted.sqlite -polygon  
\endcode

Extract the majority class in each polygon for the input land cover map. The land cover map contains five valid classes, labeled 1-5. Other class values (e.g., labeled as 0) are not taken into account in the voting.

\code
pkextractogr -i landcover.tif -s polygons.sqlite -r maxvote -o majority.sqlite -polygon -c 1 -c 2 -c 3 -c 4 -c 5
\endcode

\subsection example_pkextract_random Using random and grid samples

Extract 100 sample units following a simple random sampling design. For each sample unit, the median value is extracted from the input raster dataset in a window of 3 by 3 pixels and written to an attribute of the output vector dataset. The output vector dataset contains polygon features defined by the windows centered at the random selected sample units.

\code
pkextractogr -i input.tif -o random.sqlite -rand 100 -r median -buf 3 -polygon
\endcode

Extract points following a systematic grid with grid cell size of 100 m. Discard pixels that have a value 0 in the input raster dataset.

\code
pkextractogr -i input.tif -o systematic.sqlite -grid 100 -srcnodata 0
\endcode

\subsection example_pkextract_raster Using raster samples

Typical use where pixels are extracted based on a land cover map (sample.tif). Extract all bands for a random sample of 10 percent of the pixels in the land cover map sample.tif where the land cover classes are either 1,2 or 3 (class values). Write output to the point vector dataset extracted.sqlite.

\code
pkextractimg -i input.tif -s sample.tif -o extracted.sqlite -t 10 -c 1 -c 2 -c 3
\endcode

Extract all bands for the first 5000 pixels encountered in sample.tif where pixels have a value equal to 1. Write output to point vector dataset extracted.sqlite.

\code
pkextractimg -i input.tif -s sample.tif -o extracted.sqlite -t -5000 -c 1
\endcode

